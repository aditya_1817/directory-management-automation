import os

hostPath = "/home/aditya/Downloads/"
paths = ["/home/aditya/Downloads/images", "/home/aditya/Downloads/other", "/home/aditya/Downloads/documents", "/home/aditya/Downloads/archives", "/home/aditya/Downloads/packages"]

for x in paths:
    if not(os.path.exists(x)):
        try:
            os.mkdir(x)
        except:
            print("Something went wrong while creating directory")

if os.path.exists(hostPath):
    fileQueue = os.listdir(hostPath)

    image_formats = ['.png', '.jpg', '.jpeg', '.gif', '.psd']
    document_formats = ['.pdf', '.PDF', '.doc', 'docx', '.odt', '.xls', '.xlsx', 'ods', '.ppt', '.pptx', '.txt']
    package_formats = ['.rpm', '.deb', '.exe', '.dmg']
    archives_formats = ['.tar', '.tar.xz', '.zip', '.tar.gz', '.xz']
    other_formats = ['.php', 'html', 'htm', '.json', '.otf', '.ttf']

    for y in fileQueue:
        try:
            if any(x in y for x in image_formats):
                os.replace(hostPath+y, "/home/aditya/Downloads/images/"+y)

            if any(x in y for x in document_formats):
                os.replace(hostPath+y, "/home/aditya/Downloads/documents/"+y)

            if any(x in y for x in archives_formats):
                os.replace(hostPath+y, "/home/aditya/Downloads/archives/"+y)

            if any(x in y for x in package_formats):
                os.replace(hostPath+y, "/home/aditya/Downloads/packages/"+y)

            if any(x in y for x in other_formats):
                os.replace(hostPath+y, "/home/aditya/Downloads/other/"+y)
        
        except:
            print("Something went wrong while moving file to destination")
            
else:
    print("directory not found")
